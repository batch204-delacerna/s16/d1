// console.log("Hello World!")

//ARITHMETIC OPERATORS
let x = 1357;
let y = 7821;

// Addition Operator
let sum = x + y;
console.log("Result of Addition Operator: " + sum);

// Subtraction Operator
let difference = y - x;
console.log("Result of Subtraction Operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of Multiplication Operator: " + product);

// Division Operator
let quotient = y / x;
console.log("Result of Division Operator: " + quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of Modulo Operator: " + remainder);

// ASSIGNMENT OPERATOR
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;
console.log(assignmentNumber); // 8

// Addition Assignment Operator
// Shorthand ~~
// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber); // 10

// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

assignmentNumber -= 2; // assignmentNumber = assignmentNumber - 2;
console.log(assignmentNumber); // 8

assignmentNumber *= 2; // assignmentNumber = assignmentNumber * 2;
console.log(assignmentNumber); // 16

assignmentNumber /= 2; // assignmentNumber = assignmentNumber / 2;
console.log(assignmentNumber); // 8

// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule

	1. 3 * 4
	2. 

*/

let mdas = 1 + 2 - 3 * 4 / 5;
// let mdas = 1 + 2 - 12 / 5;
// let mdas = 1 + 2 - 12 / 5;
// let mdas = 1 + 2 - 2.4;
// let mdas = 3 - 2.4;
// let mdas = .6;
console.log("Result of the mdas operation: " + mdas); // 0.6

let pemdas = 1 + (2-3) * (4 / 5);
console.log("Result of the pemdas operation: " + pemdas); // 0.1999999999999999996

let totalPemdas = 5**2 + (10 - 2) / 2 * 3;
console.log("Result of the totalPemdas operation: " + totalPemdas); // 37

// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// Pre-Increment
// The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment"
 let increment = ++z;
console.log("Result of the pre-increment: " + increment); // 2
console.log(z);

// Post-Increment
// The value of "z" is returned and stored in the variable "increment" then the value of z increased by one.
increment = z++;
console.log("Result of the post-increment: " + increment); // 2
console.log(z);


// Pre-decrement
let decrement = --z;
console.log("Result of the post-increment: " + decrement); // 2
console.log(z);

// Post-decrement
decrement = z--;
console.log("Result of the post-increment: " + decrement); // 2
console.log(z);


// Type Coercion
/*
	Type Coercion is the automatic or implicit conversion of values from one data type to another.
	-This happens when operations are performed on different data types that would normally not be possible.


*/

/*
	-Adding/Concatenating a string and number will result a string
*/

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

// Non-Coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion); 
console.log(typeof nonCoercion);

// Adding Boolean and Number
/*
	The result is a number.
	The boolean "true" is associated with the value of 1
*/

let numE = true + 1;
console.log(numE); // 2

let numF = false + 1;
console.log(numF); // 1

// Comparison Operator
let juan = 'juan';

// Equality Operator (==);
/*
	Checks whether the operands are equal/have the same content
	Attempts to compare operands of different data types
	Return a boolean value
*/

console.log( 1 == 1); // true
console.log( 1 == 2); // false
console.log( 1 == '1'); // true
console.log( 0 == false); // true
// Compares two strings that are the same
console.log('juan' == 'juan')// true
// Compares a string with the variable "juan" declared above
console.log('juan' == juan);

// Inequality Operator (!=)
/*
	Checks whether the operand are NOT equal/have different content
*/

console.log(1!=1); //false
console.log(1!=2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false

// Strict Equality Operator (===)
/*
	-Checks whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values
*/

console.log(1===1); //true
console.log(1===2); //false
console.log(1 === '1'); //false
console.log(0 === false); //false
console.log('juan' === 'juan'); //true
console.log('juan' === juan); //true

// Strict Inequality Operator
/*
	-Checks whether the operands are NOT equal/do not have the same content
	-Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'juan'); //false
console.log('juan' !== juan); //false

// Relational Operators

let a = 50;
let b = 65;

// GT or Greater than operator ( > )
let isGreaterThan = a > b;
console.log(isGreaterThan); // false

// LT or Greater than operator ( < )
let isLessThan = a < b;
console.log(isLessThan); // true

// Greater than or Equal operator ( >= )
let isGTorEqual = a >= b;
console.log(isGTorEqual); // false

// Less than or Equal operator ( <= )
let isLTorEqual = a <= b;
console.log(isLTorEqual); // true

let numStr = "30";
console.log(a > numStr); // true

let str = "twenty";
console.log(b >= str); // false

// Logical Operator

let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator ( && - Double Ampersand )
//true && true = true
//true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

// Logical OR Operator ( || - Double Pipe )
// Returns true if one of the operands are true
// true || true = true
// true || false = true
// false || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point)
// Return the opposite value

let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet); //true

